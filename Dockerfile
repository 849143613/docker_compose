FROM openjdk:10-jre-slim
VOLUME /tmp
ADD /target/*.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]
